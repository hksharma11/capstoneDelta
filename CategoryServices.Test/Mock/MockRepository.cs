﻿using System;
using System.Collections.Generic;
using System.Text;
using CategoryServices.DataLayer;
using CategoryServices.Models;
using Moq;

namespace CategoryServices.Test.Mock
{
    public static class MockRepository
    {
        public static Mock<ICategoryService> GetCategoryService()
        {
            var carlist = new List<EcomCategory>
            {
                new EcomCategory
                {
                    CategoryId =300,
                    CategoryName = "Electronics"
                },
                new EcomCategory
                {
                    CategoryId = 300,
                    CategoryName = "Electronics"
                }

            };

            var mockrepo = new Mock<ICategoryService>();

            mockrepo.Setup(x => x.GetAllCategory()).Returns(carlist);

            return mockrepo;
        }
    }
}
