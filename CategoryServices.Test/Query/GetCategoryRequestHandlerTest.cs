﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CategoryServices.DataLayer;
using CategoryServices.Handlers;
using CategoryServices.Models;
using CategoryServices.Queries;
using Moq;
using Shouldly;
using Xunit;
using System.Linq;

namespace CategoryServices.Test.Query
{
    public class GetCategoryRequestHandlerTest
    {
        private readonly Mock<ICategoryService> _mockRepo;

        public GetCategoryRequestHandlerTest()
        {
            _mockRepo = Mock.MockRepository.GetCategoryService();
        }

        [Fact]
        public async Task GetCategoryListTest()
        {
            var handler = new GetAllCategoryHandler(_mockRepo.Object);

            var result = handler.Handle(new GetAllCategoryQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<EcomCategory>>();
        }
    }
}
